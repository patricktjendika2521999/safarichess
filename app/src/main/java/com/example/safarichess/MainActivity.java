package com.example.safarichess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button btnSingle, btnMulti;
    State state;
    ImageView imgLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgLogo = findViewById(R.id.imageView);

        state = new State();
        state.fields[0][0] = new Field("ww");
        state.fields[0][0].tipe="wakwaw";
        State test = new State();
        try {
            test = (State) state.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        //test.fields[0][0] = new Field(state.fields[0][0].tipe);
        state.fields[0][0].tipe="cukk";
        System.out.println(state.fields[0][0].tipe);
        System.out.println(test.fields[0][0].tipe);
        test.fields[0][0].tipe="fakyu";
        System.out.println(state.fields[0][0].tipe);

        btnSingle = findViewById(R.id.btnSingle);
        btnMulti = findViewById(R.id.btnMulti);

        btnSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveActivity = new Intent(getApplicationContext(), SingleActivity.class);
                startActivity(moveActivity);
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveActivity = new Intent(getApplicationContext(), MultiActivity.class);
                startActivity(moveActivity);
            }
        });
    }
}
