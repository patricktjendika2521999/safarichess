package com.example.safarichess;

import androidx.annotation.NonNull;

public class Field implements Cloneable{
    public String tipe;
    public Piece isi;
    //public int eval=0;

    public Field() {

    }
    public Field(String tipe, Piece isi) {
        this.tipe = tipe;
        this.isi = isi;
    }
//    public Field(String tipe, Piece isi, int eval) {
//        this.tipe = tipe;
//        this.isi = isi;
//        this.eval = eval;
//    }

    public Field(String tipe) {
        this.tipe = tipe;
    }

    public Field(Piece isi) {
        this.isi = isi;
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Field cloned = (Field) super.clone();
        if(cloned.isi!=null){
            cloned.isi = (Piece) cloned.isi.clone();
        }
        return cloned;
    }
}
