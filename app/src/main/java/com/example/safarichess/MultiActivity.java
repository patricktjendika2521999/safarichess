package com.example.safarichess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MultiActivity extends AppCompatActivity {
    LinearLayout linear;
    ImageButton[][] btnField = new ImageButton[9][7];
    Field[][] fields = new Field[9][7];
    LinearLayout.LayoutParams layoutParams;
    int ctrclick=0;
    String turn="blue";
    int i,j;
    int lastiRed, lastjRed, lastiBlue, lastjBlue;
    int sameRed, sameBlue;
    int ctrCheckRed, ctrCheckBlue;
    String animalRed, animalBlue;
    String currPiece;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi);
        linear = findViewById(R.id.linear);

        for (int i = 0; i < 9; i++) {
            LinearLayout rowLayout = new LinearLayout(this);
            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0,1.0f);
            rowLayout.setLayoutParams(layoutParams);
            for (int j=0;j<7;j++){
                final ImageButton imageButton = new ImageButton(this);

                if(i==0 && (j==2 || j==4) || i==1 && j==3){
                    fields[i][j] = new Field("trap_red");
                    imageButton.setBackgroundColor(getResources().getColor(R.color.colorTrap));
                    imageButton.setImageResource(R.drawable.trap1);
                }else if(i==7 && j==3 || i==8 && (j==2 || j==4)){
                    fields[i][j] = new Field("trap_blue");
                    imageButton.setBackgroundColor(getResources().getColor(R.color.colorTrap));
                    imageButton.setImageResource(R.drawable.trap1);
                }else if(i==0 && j==3){
                    fields[i][j] = new Field("goal_red");
                    imageButton.setBackgroundColor(Color.RED);
                    imageButton.setImageResource(R.drawable.star1);
                }else if(i==8 && j==3){
                    fields[i][j] = new Field("goal_blue");
                    imageButton.setBackgroundColor(Color.RED);
                    imageButton.setImageResource(R.drawable.star1);
                }else if(j>=1 && j<=2 && (i>=3 && i<=5) || j>=4 && j<=5 && (i>=3 && i<=5)){
                    fields[i][j] = new Field("water");
                    imageButton.setBackgroundResource(R.drawable.water1);
                }else{
                    fields[i][j] = new Field("grass");
                    imageButton.setBackgroundResource(R.drawable.grass1);
                }
                if(i==0 && j==0){
                    fields[i][j].isi = new Piece("lion","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==8 && j==6){
                    fields[i][j].isi = new Piece("lion","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==0 && j==6){
                    fields[i][j].isi = new Piece("tiger","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==8 && j==0){
                    fields[i][j].isi = new Piece("tiger","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==1 && j==1){
                    fields[i][j].isi = new Piece("dog","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==7 && j==5){
                    fields[i][j].isi = new Piece("dog","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==1 && j==5){
                    fields[i][j].isi = new Piece("cat","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==7 && j==1){
                    fields[i][j].isi = new Piece("cat","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==0){
                    fields[i][j].isi = new Piece("rat","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==6){
                    fields[i][j].isi = new Piece("rat","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==2){
                    fields[i][j].isi = new Piece("cheetah","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==4){
                    fields[i][j].isi = new Piece("cheetah","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==4){
                    fields[i][j].isi = new Piece("wolf","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==2){
                    fields[i][j].isi = new Piece("wolf","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==6){
                    fields[i][j].isi = new Piece("elephant","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==0){
                    fields[i][j].isi = new Piece("elephant","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                    System.out.println(fields[i][j].isi.name);
                }
                layoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                layoutParams.setMargins(10, 10, 10, 10);
                imageButton.setLayoutParams(layoutParams);
                imageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
                final int row=i;
                final int column=j;
                btnField[i][j]=imageButton;
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int i = row;
                        int j = column;
                        buttonClicked(v,i,j);
                    }
                });
                rowLayout.addView(imageButton);
            }
            linear.addView(rowLayout);
        }
    }

    public void buttonClicked(View view, int i, int j){
        //.makeText(MultiActivity.this, "i: "+i+",j: "+j, Toast.LENGTH_SHORT).show();
        if(ctrclick==0){
            if(fields[i][j].isi!=null){
                if(turn.equals("blue") && fields[i][j].isi.color.equals("blue") || turn.equals("red") && fields[i][j].isi.color.equals("red")){
                    btnField[i][j].setBackgroundColor(Color.YELLOW);
                    this.i=i;
                    this.j=j;
                    ctrclick++;
                    currPiece=fields[i][j].isi.name;
                    //System.out.println("blue!");
                }
            }
        }else if(ctrclick==1){
            btnField[this.i][this.j].setBackgroundColor(fields[this.i][this.j].isi.colorId);
            int iDiff= i-this.i;
            int jDiff= j-this.j;
            if(iDiff==0 && jDiff==0){

            }else {
                int valid=0;
                if(currPiece.equals("elephant") || currPiece.equals("wolf") || currPiece.equals("cheetah") || currPiece.equals("rat") || currPiece.equals("cat") || currPiece.equals("dog")){
                    if(isMoveOneStep(iDiff,jDiff)){
                        valid=1;
                    }
                }else if(currPiece.equals("tiger") || currPiece.equals("lion") ){
                    if(isMoveOneStep(iDiff,jDiff)){
                        valid=1;
                    }else{
                        if(this.i+1<=8){
                            if((fields[this.i+1][this.j].tipe.equals("water") && iDiff==4 && jDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.i-1>=0){
                            if((fields[this.i-1][this.j].tipe.equals("water") && iDiff==-4 && jDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.j+1<=6){
                            if((fields[this.i][this.j+1].tipe.equals("water") && jDiff==3 && iDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.j-1>=0){
                            if((fields[this.i][this.j-1].tipe.equals("water") && jDiff==-3 && iDiff==0)){
                                valid=1;
                            }
                        }
                    }
                }
                if(valid==1){
                    if(fields[i][j].isi==null){
                        if(isNotOwnGoal(i,j)){
                            if(currPiece.equals("rat")){
                                checkMove(i,j);
                            }else{
                                if(!fields[i][j].tipe.equals("water")){
                                    checkMove(i,j);
                                }
                            }
                            if(isGoal(i,j)){
                                Intent moveActivity = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(moveActivity);
                            }
                        }
                    }else if(isNotSameColor(i,j)){
                        int valid2=0;
                        if(currPiece.equals("elephant")){
                            if(!fields[i][j].tipe.equals("water") && (!fields[i][j].isi.name.equals("rat") || fields[i][j].isi.name.equals("elephant"))){
                                valid2=1;
                            }
                        }else if(currPiece.equals("rat")){
                            if(fields[i][j].isi.name.equals("elephant") || fields[i][j].isi.name.equals("rat")){
                                if(!fields[i][j].tipe.equals("water") && fields[this.i][this.j].tipe.equals("water")){

                                }else if(fields[i][j].tipe.equals("water") && !fields[this.i][this.j].tipe.equals("water")){

                                }else{
                                    valid2=1;
                                }
                            }
                        }else{
                            if(isHigherOrSameRank(i,j)){
                                valid2=1;
                            }
                        }
                        if(isTrapped(i,j)){
                            valid2=1;
                        }
                        if(valid2==1){
                            checkMove(i,j);
                        }
                    }
                }
            }
            ctrclick=0;
        }
    }

    private boolean isHigherOrSameRank(int i, int j){
        if(fields[i][j].isi.rank>=fields[this.i][this.j].isi.rank){
            return true;
        }
        return false;
    }

    private boolean isTrapped(int i, int j){
        if((fields[i][j].tipe.equals("trap_blue") && fields[i][j].isi.color.equals("red")) || (fields[i][j].tipe.equals("trap_red") && fields[i][j].isi.color.equals("blue"))){
            return true;
        }
        return false;
    }

    private boolean isNotOwnGoal(int i, int j){
        if(!(fields[i][j].tipe.equals("goal_red") && turn.equals("red")) && !(fields[i][j].tipe.equals("goal_blue") && turn.equals("blue"))){
            return true;
        }
        return false;
    }

    private boolean isMoveOneStep(int iDiff, int jDiff){
        if((iDiff+jDiff==1 || iDiff+jDiff==-1) && iDiff>=-1 && iDiff<=1 && jDiff>=-1 && jDiff<=1){
            return true;
        }
        return false;
    }

    private boolean isGoal(int i, int j){
        if(fields[i][j].tipe.equals("goal_red") || fields[i][j].tipe.equals("goal_blue")){
            return true;
        }
        return false;
    }

    private boolean isNotSameColor(int i, int j){
        if(!fields[this.i][this.j].isi.color.equals(fields[i][j].isi.color)){
            return true;
        }
        return  false;
    }

    private void checkMove(int i, int j){
        if(turn.equals("blue")){
            if(ctrCheckBlue==2 && sameBlue==3 && lastiBlue==i && lastjBlue==j && fields[this.i][this.j].isi.name.equals(animalBlue)){

            }else{
                move(i,j);
                if(!fields[i][j].isi.name.equals(animalBlue)){
                    sameBlue=0;
                    ctrCheckBlue=0;
                }
                if(sameBlue==3 && ctrCheckBlue==2){
                    sameBlue=0;
                    ctrCheckBlue=0;
                }
                fields[this.i][this.j].isi = null;
                if(ctrCheckBlue==2){
                    if(lastiBlue==i && lastjBlue==j){
                        sameBlue++;
                    }else{
                        sameBlue=0;
                    }
                    ctrCheckBlue=0;
                }
                ctrCheckBlue++;
                if(ctrCheckBlue==1){
                    animalBlue = fields[i][j].isi.name;
                    lastiBlue= i;
                    lastjBlue=j;
                    if(sameBlue==0){
                        sameBlue++;
                    }
                }
                turn = "red";
            }
        }else {
            if(ctrCheckRed==2 && sameRed==3 && lastiRed==i && lastjRed==j  && fields[this.i][this.j].isi.name.equals(animalRed)){

            }else{
                move(i,j);
                if(!fields[i][j].isi.name.equals(animalRed)){
                    sameRed=0;
                    ctrCheckRed=0;
                }
                if(sameRed==3  && ctrCheckRed==2){
                    sameRed=0;
                    ctrCheckRed=0;
                }
                if(ctrCheckRed==2){
                    if(lastiRed==i && lastjRed==j){
                        sameRed++;
                    }else{
                        sameRed=0;
                    }
                    ctrCheckRed=0;
                }
                ctrCheckRed++;
                if(ctrCheckRed==1){
                    animalRed = fields[i][j].isi.name;
                    lastiRed= i;
                    lastjRed=j;
                    if(sameRed==0){
                        sameRed++;
                    }
                }
                turn = "blue";
            }
        }
    }

    private void move(int i, int j){
        btnField[i][j].setImageResource(fields[this.i][this.j].isi.img);
        btnField[i][j].setBackgroundColor(fields[this.i][this.j].isi.colorId);
        defaultResField(this.i,this.j);
        try {
            fields[i][j].isi= (Piece) fields[this.i][this.j].isi.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        fields[this.i][this.j].isi = null;
    }

    private void defaultResField(int i, int j){
        btnField[i][j].setImageResource(android.R.color.transparent);
        if(i==0 && (j==2 || j==4) || i==1 && j==3 || i==7 && j==3 || i==8 && (j==2 || j==4)){
            btnField[i][j].setBackgroundColor(getResources().getColor(R.color.colorTrap));
            btnField[i][j].setImageResource(R.drawable.trap1);
        }else if(i==0 && j==3 || i==8 && j==3){
            btnField[i][j].setBackgroundColor(Color.RED);
            btnField[i][j].setImageResource(R.drawable.star1);
        }else if(j>=1 && j<=2 && (i>=3 && i<=5) || j>=4 && j<=5 && (i>=3 && i<=5)){
            btnField[i][j].setBackgroundResource(R.drawable.water1);
        }else{
            btnField[i][j].setBackgroundResource(R.drawable.grass1);
        }
    }
}
