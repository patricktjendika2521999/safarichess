package com.example.safarichess;

import androidx.annotation.NonNull;
import com.google.gson.Gson;

public class State implements Cloneable{
    public Field[][] fields;
    public double eval=0;

    public State() {
        fields = new Field[9][7];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {
                fields[i][j] = new Field();
            }
        }
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        State cloned = (State) super.clone();
        //cloned.fields = fields.clone();
//        for (int i = 0; i < 9; i++) {
//            for (int j = 0; j < 7; j++) {
//                cloned.fields[i][j]=new Field(cloned.fields[i][j].tipe);
//            }
//        }
        //cloned.fields = cloned.fields.clone();
        return cloned;
    }
}
