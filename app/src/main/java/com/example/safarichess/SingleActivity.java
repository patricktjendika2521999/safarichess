package com.example.safarichess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SingleActivity extends AppCompatActivity {
    Button btnEasy, btnMedium, btnHard;
    String[] waw = new String[10];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        waw[0]="555";
        String[] eda;
        eda = waw.clone();
        waw[0]="1";
        System.out.println("waw: "+waw[0]);
        System.out.println("eda: "+eda[0]);
        btnEasy = findViewById(R.id.btnEasy);
        btnMedium = findViewById(R.id.btnMedium);
        btnHard = findViewById(R.id.btnHard);

        btnEasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveActivity = new Intent(getApplicationContext(), EasyActivity.class);
                moveActivity.putExtra("difficulty","easy");
                startActivity(moveActivity);
            }
        });

        btnMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveActivity = new Intent(getApplicationContext(), EasyActivity.class);
                moveActivity.putExtra("difficulty","medium");
                startActivity(moveActivity);
            }
        });

        btnHard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveActivity = new Intent(getApplicationContext(), EasyActivity.class);
                moveActivity.putExtra("difficulty","hard");
                startActivity(moveActivity);
            }
        });
    }
}
