package com.example.safarichess;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import androidx.annotation.NonNull;

public class Piece implements Cloneable{
    public String name;
    public int rank;
    public int img;
    public String color;
    public int colorId;
    //public Context context;

    public Piece(String name, String color) {
        this.name = name;
        this.color = color;
        if(color.equals("red")){
            this.colorId = Color.RED;
        }else{
            this.colorId = Color.BLUE;
        }
        if(name.equals("elephant")){
            this.rank = 1;
            img = R.drawable.elephant;
        }else if(name.equals("lion")){
            this.rank = 2;
            img = R.drawable.lion;
        }else if(name.equals("tiger")){
            this.rank = 3;
            img = R.drawable.tiger;
        }else if(name.equals("cheetah")){
            this.rank = 4;
            img = R.drawable.cheetah;
        }else if(name.equals("wolf")){
            this.rank = 5;
            img = R.drawable.wolf;
        }else if(name.equals("dog")){
            this.rank = 6;
            img = R.drawable.dog;
        }else if(name.equals("cat")){
            this.rank = 7;
            img = R.drawable.cat;
        }else if(name.equals("rat")){
            this.rank = 8;
            img = R.drawable.rat;
        }
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
