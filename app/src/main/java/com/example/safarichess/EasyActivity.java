package com.example.safarichess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class EasyActivity extends AppCompatActivity {
    LinearLayout linear;
    ImageButton[][] btnField = new ImageButton[9][7];
    State state;
    Field[][] fields = new Field[9][7];
    LinearLayout.LayoutParams layoutParams;
    int ctrclick=0;
    String turn="blue";
    int i,j;
    int lastiRed, lastjRed, lastiBlue, lastjBlue;
    int sameRed, sameBlue;
    int ctrCheckRed, ctrCheckBlue;
    String animalRed, animalBlue;
    String currPiece;
    ArrayList<State> open = new ArrayList<>();
    String difficulty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi);

        Intent terima = getIntent();
        if(terima.getExtras()!=null){
            difficulty = terima.getStringExtra("difficulty");
        }

        linear = findViewById(R.id.linear);

        for (int i = 0; i < 9; i++) {
            LinearLayout rowLayout = new LinearLayout(this);
            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0,1.0f);
            rowLayout.setLayoutParams(layoutParams);
            for (int j=0;j<7;j++){
                final ImageButton imageButton = new ImageButton(this);

                if(i==0 && (j==2 || j==4) || i==1 && j==3){
                    fields[i][j] = new Field("trap_red");
                    imageButton.setBackgroundColor(getResources().getColor(R.color.colorTrap));
                    imageButton.setImageResource(R.drawable.trap1);
                }else if(i==7 && j==3 || i==8 && (j==2 || j==4)){
                    fields[i][j] = new Field("trap_blue");
                    imageButton.setBackgroundColor(getResources().getColor(R.color.colorTrap));
                    imageButton.setImageResource(R.drawable.trap1);
                }else if(i==0 && j==3){
                    fields[i][j] = new Field("goal_red");
                    imageButton.setBackgroundColor(Color.RED);
                    imageButton.setImageResource(R.drawable.star1);
                }else if(i==8 && j==3){
                    fields[i][j] = new Field("goal_blue");
                    imageButton.setBackgroundColor(Color.RED);
                    imageButton.setImageResource(R.drawable.star1);
                }else if(j>=1 && j<=2 && (i>=3 && i<=5) || j>=4 && j<=5 && (i>=3 && i<=5)){
                    fields[i][j] = new Field("water");
                    imageButton.setBackgroundResource(R.drawable.water1);
                }else{
                    fields[i][j] = new Field("grass");
                    imageButton.setBackgroundResource(R.drawable.grass1);
                }
                if(i==0 && j==0){
                    fields[i][j].isi = new Piece("lion","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==8 && j==6){
                    fields[i][j].isi = new Piece("lion","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==0 && j==6){
                    fields[i][j].isi = new Piece("tiger","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==8 && j==0){
                    fields[i][j].isi = new Piece("tiger","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==1 && j==1){
                    fields[i][j].isi = new Piece("dog","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==7 && j==5){
                    fields[i][j].isi = new Piece("dog","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==1 && j==5){
                    fields[i][j].isi = new Piece("cat","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==7 && j==1){
                    fields[i][j].isi = new Piece("cat","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==0){
                    fields[i][j].isi = new Piece("rat","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==6){
                    fields[i][j].isi = new Piece("rat","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==2){
                    fields[i][j].isi = new Piece("cheetah","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==4){
                    fields[i][j].isi = new Piece("cheetah","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==4){
                    fields[i][j].isi = new Piece("wolf","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==2){
                    fields[i][j].isi = new Piece("wolf","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==2 && j==6){
                    fields[i][j].isi = new Piece("elephant","red");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }else if(i==6 && j==0){
                    fields[i][j].isi = new Piece("elephant","blue");
                    imageButton.setImageResource(fields[i][j].isi.img);
                    imageButton.setBackgroundColor(fields[i][j].isi.colorId);
                }
                layoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                layoutParams.setMargins(10, 10, 10, 10);
                imageButton.setLayoutParams(layoutParams);
                imageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
                final int row=i;
                final int column=j;
                btnField[i][j]=imageButton;
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int i = row;
                        int j = column;
                        buttonClicked(v,i,j);
                    }
                });
                rowLayout.addView(imageButton);
            }
            linear.addView(rowLayout);
        }
    }

    public void buttonClicked(View view, int i, int j){
        //.makeText(MultiActivity.this, "i: "+i+",j: "+j, Toast.LENGTH_SHORT).show();
        if(ctrclick==0){
            if(fields[i][j].isi!=null){
                if(turn.equals("blue") && fields[i][j].isi.color.equals("blue")){
                    btnField[i][j].setBackgroundColor(Color.YELLOW);
                    this.i=i;
                    this.j=j;
                    ctrclick++;
                    currPiece=fields[i][j].isi.name;
                    //System.out.println("blue!");
                }
            }
        }else if(ctrclick==1){
            btnField[this.i][this.j].setBackgroundColor(fields[this.i][this.j].isi.colorId);
            int iDiff= i-this.i;
            int jDiff= j-this.j;
            if(iDiff==0 && jDiff==0){

            }else {
                int valid=0;
                if(currPiece.equals("elephant") || currPiece.equals("wolf") || currPiece.equals("cheetah") || currPiece.equals("rat") || currPiece.equals("cat") || currPiece.equals("dog")){
                    if(isMoveOneStep(iDiff,jDiff)){
                        valid=1;
                    }
                }else if(currPiece.equals("tiger") || currPiece.equals("lion") ){
                    if(isMoveOneStep(iDiff,jDiff)){
                        valid=1;
                    }else{
                        if(this.i+1<=8){
                            if((fields[this.i+1][this.j].tipe.equals("water") && iDiff==4 && jDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.i-1>=0){
                            if((fields[this.i-1][this.j].tipe.equals("water") && iDiff==-4 && jDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.j+1<=6){
                            if((fields[this.i][this.j+1].tipe.equals("water") && jDiff==3 && iDiff==0)){
                                valid=1;
                            }
                        }
                        if(this.j-1>=0){
                            if((fields[this.i][this.j-1].tipe.equals("water") && jDiff==-3 && iDiff==0)){
                                valid=1;
                            }
                        }
                    }
                }
                if(valid==1){
                    if(fields[i][j].isi==null){
                        if(isNotOwnGoal(i,j)){
                            if(currPiece.equals("rat")){
                                checkMove(i,j);
                            }else{
                                if(!fields[i][j].tipe.equals("water")){
                                    checkMove(i,j);
                                }
                            }
                        }
                    }else if(isNotSameColor(i,j)){
                        int valid2=0;
                        if(currPiece.equals("elephant")){
                            if(!fields[i][j].tipe.equals("water") && (!fields[i][j].isi.name.equals("rat") || fields[i][j].isi.name.equals("elephant"))){
                                valid2=1;
                            }
                        }else if(currPiece.equals("rat")){
                            if(fields[i][j].isi.name.equals("elephant") || fields[i][j].isi.name.equals("rat")){
                                if(!fields[i][j].tipe.equals("water") && fields[this.i][this.j].tipe.equals("water")){

                                }else if(fields[i][j].tipe.equals("water") && !fields[this.i][this.j].tipe.equals("water")){

                                }else{
                                    valid2=1;
                                }
                            }
                        }else{
                            if(isHigherOrSameRank(i,j)){
                                valid2=1;
                            }
                        }
                        if(isTrapped(i,j)){
                            valid2=1;
                        }
                        if(valid2==1){
                            checkMove(i,j);
                        }
                    }
                }
            }
            ctrclick=0;
        }
    }
    State stateX;

    private void step(Field[][] fields, String turn){
        this.fields=fields;
        for (int i = 0; i < 9; i++) {
            for (int j=0;j<7;j++){
                if(fields[i][j].isi!=null){
                    if(fields[i][j].isi.color.equals(turn)){
                        //System.out.println(fields[i][j].isi.name);
                        currPiece=fields[i][j].isi.name;
                        this.i=i;
                        this.j=j;
                        for(int k=0;k<4;k++){
                            int ii=this.i;
                            int jj=this.j;
                            int iDiff=0;
                            int jDiff=0;
                            int valid=0;
                            int check=0;
                            if(fields[i][j].isi.name.equals("elephant") || fields[i][j].isi.name.equals("wolf") || fields[i][j].isi.name.equals("cheetah") || fields[i][j].isi.name.equals("rat") || fields[i][j].isi.name.equals("cat") || fields[i][j].isi.name.equals("dog")){
                                if(k==0){
                                    if(i!=0){
                                        //System.out.println("atas");
                                        iDiff=-1;
                                        check=1;
                                    }
                                }else if(k==1){
                                    if(j!=6){
                                        //System.out.println("kanan");
                                        jDiff=1;
                                        check=1;
                                    }
                                }else if(k==3){
                                    if(i!=8){
                                        //System.out.println("bawah");
                                        iDiff=1;
                                        check=1;
                                    }
                                }else if(k==2){
                                    if(j!=0){
                                        //System.out.println("kiri");
                                        jDiff=-1;
                                        check=1;
                                    }
                                }
                            }else{
                                if(k==0){
                                    if(i!=0){
                                        //System.out.println("atas");
                                        if(fields[this.i-1][this.j].tipe.equals("water")){
                                            valid=1;
                                            iDiff=-4;
                                        }else{
                                            iDiff=-1;
                                        }
                                        check=1;
                                    }
                                }else if(k==1){
                                    if(j!=6){
                                        //System.out.println("kanan");
                                        if(fields[this.i][this.j+1].tipe.equals("water")){
                                            valid=1;
                                            jDiff=3;
                                        }else{
                                            jDiff=1;
                                        }
                                        check=1;
                                    }
                                }else if(k==3){
                                    if(i!=8){
                                        //System.out.println("bawah");
                                        if(fields[this.i+1][this.j].tipe.equals("water")){
                                            valid=1;
                                            iDiff=4;
                                        }else{
                                            iDiff=1;
                                        }
                                        check=1;
                                    }
                                }else if(k==2){
                                    if(j!=0){
                                        //System.out.println("kiri");
                                        if(fields[this.i][this.j-1].tipe.equals("water")){
                                            valid=1;
                                            jDiff=-3;
                                        }else{
                                            jDiff=-1;
                                        }
                                        check=1;
                                    }
                                }
                            }
                            ii += iDiff;
                            jj += jDiff;
                            if(check==1){
                                if(isMoveOneStep(iDiff,jDiff)){
                                    valid=1;
                                }
                                if(valid==1){
                                    //System.out.println("valid");
                                    //System.out.println("this.i"+this.i);
                                    //System.out.println("this.j"+this.j);
                                    if(fields[ii][jj].isi==null){
                                        if(isNotOwnGoal(ii,jj)){
                                            if(currPiece.equals("rat")){
                                                //System.out.println("move(rat)");
                                                checkMove2(ii,jj,turn);
                                            }else{
                                                if(!fields[ii][jj].tipe.equals("water")){
                                                    //System.out.println("move(!rat)");
                                                    checkMove2(ii,jj,turn);
                                                }
                                            }
                                        }
                                    }else if(isNotSameColor(ii,jj)){
                                        int valid2=0;
                                        if(currPiece.equals("elephant")){
                                            if(!fields[ii][jj].tipe.equals("water") && (!fields[ii][jj].isi.name.equals("rat") || fields[ii][jj].isi.name.equals("elephant"))){
                                                valid2=1;
                                            }
                                        }else if(currPiece.equals("rat")){
                                            if(fields[ii][jj].isi.name.equals("elephant") || fields[ii][jj].isi.name.equals("rat")){
                                                if(!fields[ii][jj].tipe.equals("water") && fields[this.i][this.j].tipe.equals("water")){

                                                }else if(fields[ii][jj].tipe.equals("water") && !fields[this.i][this.j].tipe.equals("water")){

                                                }else{
                                                    valid2=1;
                                                }
                                            }
                                        }else{
                                            if(isHigherOrSameRank(ii,jj)){
                                                if(currPiece.equals("dog")){
                                                    //System.out.println("masuk higher or same");
                                                }
                                                valid2=1;
                                            }
                                        }
                                        //System.out.println("currPiece: "+currPiece);
                                        //System.out.println("musuh :"+fields[ii][jj].isi.name);
                                        if(isTrapped(ii,jj)){
                                            //System.out.println("currPiece 2: "+currPiece);
                                            if(currPiece.equals("dog")){
                                                //System.out.println("masuk trapped");
                                            }
                                            valid2=1;
                                        }
                                        if(valid2==1){
                                            checkMove2(ii,jj,turn);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //System.out.println("open.size: "+open.size());
        //System.out.println("6,5: "+open.get(0).fields[6][5].isi.name);

//        if(!open.isEmpty()){
//            if(open.size()==5){
//                System.out.println("masuk ke 1");
//                this.fields = open.get(1).fields;
//            }else{
//                System.out.println(currPiece);
//                System.out.println("masuk ke 0");
//                this.fields = open.get(0).fields;
//            }
//        }
        //System.out.println("6,5 ke-2: "+fields[6][5].isi.name);
        //System.out.println("0,1: "+fields[0][1].isi.name);

    }
    int tracing=0;
    String action="";

    private double minimax(Field[][] root, int depth, int maximize, double a, double b){
        //System.out.println("minimax");
        int ctr=0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {
                if (root[i][j].isi != null) {
                    if(maximize==1){
                        if (root[i][j].isi.color.equals("blue")) {
                            ctr++;
                        }
                    }else{
                        if (root[i][j].isi.color.equals("red")) {
                            ctr++;
                        }
                    }
                }
            }
        }
        ArrayList<State> open = new ArrayList<>();
        if(ctr==0 || depth==0){
//            Random random = new Random();
            //int angka = random.nextInt(100);
//            System.out.println("angka: "+angka);
            //System.out.println("random: "+random.nextInt());
            //return angka;
            System.out.println("Action: "+action);
            double angka = evaluation_function(root);
            //System.out.println("angka: "+angka);
            return angka;
        }else{
            if(maximize==0){
                tracing=1;
                System.out.println("Minimizing");
                this.open.clear();
                step(root,"blue");
                //open = this.open;
                for (int k = 0; k < this.open.size(); k++) {
                    try {
                        open.add((State) this.open.get(k).clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
                //open.addAll(this.open);
                for (int k = 0; k < open.size(); k++) {
                    for (int i = 0; i < 9; i++) {
                        for (int j = 0; j < 7; j++) {
                            open.get(k).fields[i][j] = new Field(open.get(k).fields[i][j].tipe, open.get(k).fields[i][j].isi);
                        }
                    }
                }
                //System.out.println("jumlah open (min): "+open.size());
                double bestval=Integer.MAX_VALUE;
                for (int i = 0; i < open.size(); i++) {
                    //open.get(i).eval=Integer.MAX_VALUE;
                    action="Minimize";
                    double value = minimax(open.get(i).fields,depth-1,1,a,b);
                    //System.out.println("value: "+value);
                    open.get(i).eval=value;
                    bestval=Math.min(bestval,value);
                    if(value<=bestval){
                        open.get(i).eval=value;
                    }
                    b = Math.min(b,value);
                    //open.get(i).eval=b;
                    if(b<=a){
                        break;
                    }
                }
                double min = Integer.MAX_VALUE;
                for (int i = 0; i < open.size(); i++) {
                    if(open.get(i).eval<=min){
                        this.fields = open.get(i).fields;
                        min = open.get(i).eval;
                    }
                }
                if(b!=0){
                    //System.out.println("return b: "+b);
                }else{
                    //System.out.println("return b=0: "+b);
                }
                return bestval;
            }else{
                System.out.println("Maximizing");
                this.open.clear();
                step(root,"red");
                for (int k = 0; k < this.open.size(); k++) {
                    try {
                        open.add((State) this.open.get(k).clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
                //open.addAll(this.open);
                for (int k = 0; k < open.size(); k++) {
                    for (int i = 0; i < 9; i++) {
                        for (int j = 0; j < 7; j++) {
                            open.get(k).fields[i][j] = new Field(open.get(k).fields[i][j].tipe, open.get(k).fields[i][j].isi);
                        }
                    }
                }
                //System.out.println("jumlah open (max): "+open.size());
                double bestval = Integer.MIN_VALUE;
                for (int i = 0; i < open.size(); i++) {
                    //open.get(i).eval=Integer.MIN_VALUE;
                    action="Maximize";
                    double value = minimax(open.get(i).fields,depth-1,0,a,b);
                    if(value!=0){
                        //System.out.println("max value: "+value);
                    }
                    //System.out.println("open.get(i).eval: "+open.get(i).eval);
                    open.get(i).eval=value;
                    bestval=Math.max(bestval,value);
                    if(value>=bestval){
                        open.get(i).eval=value;
                    }
                    a = Math.max(a,value);
                    //open.get(i).eval=a;
                    if(a>=b){
                        break;
                    }
                }
                double max = Integer.MIN_VALUE;
                for (int i = 0; i < open.size(); i++) {
                    //System.out.println("i minimax: "+i);
                    //System.out.println("geteval: "+open.get(i).eval);
                    if(open.get(i).eval>=max){
                        this.fields = open.get(i).fields;
                        max = open.get(i).eval;
                        if(max!=0){
                            //System.out.println("max anj: "+max);
                        }
                    }
                }
                if(max==0){
                    //System.out.println("Semua 0");
                }
                return bestval;
            }
            //step(root,);
//            for (int i = 0; i < ; i++) {
//
//            }
        }
        //return root;
    }
    int ctr_eval=0;

    private double evaluation_function(Field[][] fields){
        ctr_eval++;
        //System.out.println("ctr_eval: "+ctr_eval);
        int countRed=0,countBlue=0;
        //c(jumlah piece), v(value per piece)
        double c=0, v=0, d=0, d2=0;
        double cat=0,dog=0;
        double rat_elp=0;
        double diff_enemy=0;
        String elp="null";
        double[][] coor_value = {
                {0, 4, 2, Integer.MIN_VALUE, 2, 4, 0},
                {0, 3, 5, 2, 5, 3, 0},
                {2, 2, 3, 2, 3, 2, 2},
                {2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5},
                {2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5},
                {2.5, 2.5, 2,5, 2.5, 2.5, 2.5, 2.5},
                {3, 3, 3, 4, 3, 3, 3},
                {3, 3, 5, 100, 5, 3, 3},
                {3, 4, 100, Integer.MAX_VALUE, 100, 4, 3},
        };;

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {

                if (fields[i][j].isi != null) {
                    int angka=0;
                    if(fields[i][j].isi.name.equals("elephant")){
                        System.out.print("el");
                        elp="exist";
                        angka = 8;
                    }else if(fields[i][j].isi.name.equals("rat")){
                        System.out.print("ra");
                        angka = 7;
                    }else if(fields[i][j].isi.name.equals("lion")){
                        System.out.print("li");
                        angka = 6;
                    }else if(fields[i][j].isi.name.equals("tiger")){
                        System.out.print("ti");
                        angka = 5;
                    }else if(fields[i][j].isi.name.equals("cheetah")){
                        System.out.print("ch");
                        angka = 4;
                    }else if(fields[i][j].isi.name.equals("wolf")){
                        System.out.print("wo");
                        angka = 3;
                    }else if(fields[i][j].isi.name.equals("dog")){
                        System.out.print("do");
                        angka = 2;
                    }else if(fields[i][j].isi.name.equals("cat")){
                        System.out.print("ca");
                        angka = 1;
                    }
                    if (fields[i][j].isi.color.equals("red")) {
                        d+=coor_value[i][j];
                        countRed++;
                    }else if (fields[i][j].isi.color.equals("blue")){
                        countBlue++;
                        angka*=-1;
                    }
                    v+=angka;
                }else{
                    System.out.print("##");
                }
            }
            System.out.println();
        }
        if(fields[1][2].isi !=null && fields[1][2].isi.color.equals("red") && (fields[1][2].isi.name.equals("dog") || fields[1][2].isi.name.equals("cat"))){

            d+=5;
        }
        if(fields[1][4].isi !=null && fields[1][4].isi.color.equals("red") && (fields[1][4].isi.name.equals("dog") || fields[1][4].isi.name.equals("cat"))){

            d+=5;
        }
        if(fields[1][2].isi ==null){
            d-=3;
        }
        if(fields[1][4].isi ==null){
            d-=3;
        }
        if(fields[1][2].isi !=null && fields[1][2].isi.color.equals("blue")){
            d-=10;
        }
        if(fields[1][4].isi !=null && fields[1][4].isi.color.equals("blue")){
            d-=10;
        }
        if(fields[0][1].isi !=null && fields[0][1].isi.color.equals("blue")){
            d-=100;
        }
        if(fields[0][5].isi !=null && fields[0][5].isi.color.equals("blue")){
            d-=100;
        }

        if(fields[0][1].isi !=null && fields[0][1].isi.color.equals("red") && (fields[0][1].isi.name.equals("wolf") || fields[0][1].isi.name.equals("cheetah"))){

            d2+=4;
        }
        if(fields[1][1].isi !=null && fields[1][1].isi.color.equals("red") && (fields[1][1].isi.name.equals("wolf") || fields[1][1].isi.name.equals("cheetah"))){

            d2+=20;
        }
        if(fields[0][5].isi !=null && fields[0][5].isi.color.equals("red") && (fields[0][5].isi.name.equals("wolf") || fields[0][5].isi.name.equals("cheetah"))){

            d2+=4;
        }
        if(fields[1][5].isi !=null && fields[1][5].isi.color.equals("red") && (fields[1][5].isi.name.equals("wolf") || fields[1][5].isi.name.equals("cheetah"))){

            d2+=20;
        }

        if(fields[1][2].isi !=null && fields[1][2].isi.color.equals("red") && (fields[1][2].isi.name.equals("wolf") || fields[1][2].isi.name.equals("cheetah"))){

            d2+=20;
        }

        if(fields[1][4].isi !=null && fields[1][4].isi.color.equals("red") && (fields[1][4].isi.name.equals("wolf") || fields[1][4].isi.name.equals("cheetah"))){

            d2+=20;
        }

        if(fields[0][1].isi !=null && fields[0][1].isi.color.equals("red") && (fields[0][1].isi.name.equals("dog") || fields[0][1].isi.name.equals("cat"))){
            d2+=20;
        }
        if(fields[0][5].isi !=null && fields[0][5].isi.color.equals("red") && (fields[0][5].isi.name.equals("dog") || fields[0][5].isi.name.equals("cat"))){
            d2+=20;
        }

        if(fields[1][1].isi !=null && fields[1][1].isi.color.equals("red") && (fields[1][1].isi.name.equals("dog") || fields[1][1].isi.name.equals("cat"))){

            d2+=4;
        }
        if(fields[1][5].isi !=null && fields[1][5].isi.color.equals("red") && (fields[1][5].isi.name.equals("dog") || fields[1][5].isi.name.equals("cat"))){

            d2+=4;
        }
        if(fields[2][1].isi !=null && fields[2][1].isi.color.equals("red") && (fields[2][1].isi.name.equals("dog") || fields[2][1].isi.name.equals("cat"))){

            d2+=4;
        }
        if(fields[2][5].isi !=null && fields[2][5].isi.color.equals("red") && (fields[2][5].isi.name.equals("dog") || fields[2][5].isi.name.equals("cat"))){

            d2+=4;
        }

        if(fields[2][1].isi !=null && fields[2][1].isi.color.equals("red") && (fields[2][1].isi.name.equals("lion") || fields[2][1].isi.name.equals("tiger"))){

            d2+=4;
        }
        if(fields[2][2].isi !=null && fields[2][2].isi.color.equals("red") && (fields[2][2].isi.name.equals("lion") || fields[2][2].isi.name.equals("tiger"))){

            d2+=4;
        }

        if(fields[2][4].isi !=null && fields[2][4].isi.color.equals("red") && (fields[2][4].isi.name.equals("lion") || fields[2][4].isi.name.equals("tiger"))){

            d2+=4;
        }
        if(fields[2][5].isi !=null && fields[2][5].isi.color.equals("red") && (fields[2][5].isi.name.equals("lion") || fields[2][5].isi.name.equals("tiger"))){

            d2+=4;
        }

        int i_rat=0;
        int j_rat=0;
        int i_elp=0;
        int j_elp=0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {
                if(fields[i][j].isi !=null && fields[i][j].isi.color.equals("blue") && fields[i][j].isi.name.equals("rat")){
                    i_rat=i;
                    j_rat=j;
                }else if(fields[i][j].isi !=null && fields[i][j].isi.color.equals("red") && fields[i][j].isi.name.equals("elephant")){
                    if(i==2){
                        if(j==2 || j==4){
                            d2+=10;
                        }
                        d2+=20;
                    }

                    i_elp=i;
                    j_elp=j;
                }
            }
        }
        double diff_rat_elp=Math.abs(i_rat-i_elp)*1.5+Math.abs(j_rat-j_elp);
        rat_elp=diff_rat_elp*1;

        int i_enemy=8;
        int i_lion=0;
        int j_lion=0;
        int i_tiger=0;
        int j_tiger=0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {
                if(fields[i][j].isi !=null && fields[i][j].isi.color.equals("red") && fields[i][j].isi.name.equals("lion")){
                    i_lion=i;
                    j_lion=j;
                }else if(fields[i][j].isi !=null && fields[i][j].isi.color.equals("red") && fields[i][j].isi.name.equals("tiger")){
                    i_tiger=i;
                    j_tiger=j;
                }
            }
        }
        double diff_tiger = Math.abs(i_enemy-i_tiger)*5;
        diff_enemy=Math.abs(i_enemy-i_lion)*5;
        double diff_elephant = Math.abs(i_enemy-i_elp)*5;


        //System.out.println("countRed:"+countRed);
        //System.out.println("countBlue:"+countBlue);
        if(countRed > countBlue){
            c = (100.0 * countRed)/(countRed + countBlue);
        }else if(countBlue > countRed){
            System.out.println("blue banyak");
            c = -(100.0 * countBlue)/(countRed + countBlue);
        }else{
            c = 0;
        }
        if(tracing==1){
            if(c<0){
                //System.out.println("c: "+c);
            }else{
                //System.out.println("c>=0:"+c);
            }
        }


        double score = (100 * c)+(100 * v)+(10 * d)+(20 * d2)+(100 * rat_elp)+(-100* diff_enemy)+(-25* diff_tiger)+(-10*diff_elephant);
        System.out.println("Score: "+score);
        return score;
    }

    private boolean isHigherOrSameRank(int i, int j){
        if(fields[i][j].isi.rank>=fields[this.i][this.j].isi.rank){
            return true;
        }
        return false;
    }

    private boolean isTrapped(int i, int j){
        //System.out.println("checking isTrapped!");
        //System.out.println("tipe land: "+fields[i][j].tipe);
        //System.out.println("isi.color: "+fields[i][j].isi.color);
        if((fields[i][j].tipe.equals("trap_blue") && fields[i][j].isi.color.equals("red")) || (fields[i][j].tipe.equals("trap_red") && fields[i][j].isi.color.equals("blue"))){
            return true;
        }
        return false;
    }

    private boolean isNotOwnGoal(int i, int j){
        if(!(fields[i][j].tipe.equals("goal_red") && turn.equals("red")) && !(fields[i][j].tipe.equals("goal_blue") && turn.equals("blue"))){
            return true;
        }
        return false;
    }

    private boolean isMoveOneStep(int iDiff, int jDiff){
        if((iDiff+jDiff==1 || iDiff+jDiff==-1) && iDiff>=-1 && iDiff<=1 && jDiff>=-1 && jDiff<=1){
            return true;
        }
        return false;
    }

    private boolean isGoal(int i, int j){
        if(fields[i][j].tipe.equals("goal_red") || fields[i][j].tipe.equals("goal_blue")){
            return true;
        }
        return false;
    }

    private boolean isNotSameColor(int i, int j){
        if(!fields[this.i][this.j].isi.color.equals(fields[i][j].isi.color)){
            return true;
        }
        return false;
    }
    Map<String, Integer> map = new HashMap<String, Integer>();
    private void checkMove(int i, int j){
        if(turn.equals("blue")){
            if(ctrCheckBlue==2 && sameBlue==3 && lastiBlue==i && lastjBlue==j && fields[this.i][this.j].isi.name.equals(animalBlue)){

            }else{
                move(i,j);
                if(!fields[i][j].isi.name.equals(animalBlue)){
                    sameBlue=0;
                    ctrCheckBlue=0;
                }
                if(sameBlue==3 && ctrCheckBlue==2){
                    sameBlue=0;
                    ctrCheckBlue=0;
                }
                fields[this.i][this.j].isi = null;
                if(ctrCheckBlue==2){
                    if(lastiBlue==i && lastjBlue==j){
                        sameBlue++;
                    }else{
                        sameBlue=0;
                    }
                    ctrCheckBlue=0;
                }
                ctrCheckBlue++;
                if(ctrCheckBlue==1){
                    animalBlue = fields[i][j].isi.name;
                    lastiBlue= i;
                    lastjBlue=j;
                    if(sameBlue==0){
                        sameBlue++;
                    }
                }
                turn = "red";
                if(isGoal(i,j)){
                    Intent moveActivity = new Intent(getApplicationContext(), MainActivity.class);
                    Toast.makeText(this, "Blue Win!", Toast.LENGTH_SHORT).show();
                    startActivity(moveActivity);
                }
                //step(fields,"red");
                if(difficulty.equals("easy")){
                    minimax(fields,2,1, Integer.MIN_VALUE, Integer.MAX_VALUE);
                }else if(difficulty.equals("medium")){
                    minimax(fields,3,1, Integer.MIN_VALUE, Integer.MAX_VALUE);
                }else{
                    minimax(fields,4,1, Integer.MIN_VALUE, Integer.MAX_VALUE);
                }

                refreshField();
                if(sameRed==3 && ctrCheckRed==2){
                    sameRed=0;
                    ctrCheckRed=0;
                }
                if(ctrCheckRed==2){
                    int ctr=0;
                    Map<String, Integer> map = new HashMap<String, Integer>();
                    for (int k = 0; k < 9; k++) {
                        for (int l = 0; l < 7; l++) {
                            if(fields[k][l].isi!=null) {
                                if (fields[k][l].isi.color.equals("red")) {
                                    //System.out.println(fields[k][l].isi.name);
                                    map.put(fields[k][l].isi.name, ctr);
                                    //map.get("name"); // returns "demo"
                                }
                            }
                            ctr++;
                            //state.fields[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
                        }
                    }

//                    for (Map.Entry<String,Integer> entry : map.entrySet())
//                        System.out.println("Key = " + entry.getKey() +
//                                ", Value = " + entry.getValue());
//
//                    if(this.map!=null){
//                        System.out.println("this.map");
//                        for (Map.Entry<String,Integer> entry : this.map.entrySet())
//                            System.out.println("Key = " + entry.getKey() +
//                                    ", Value = " + entry.getValue());
//                    }

                    if(this.map.equals(map)){
                        //System.out.println("SAMA!");
                        sameRed++;
                    }else{
                        //System.out.println("TIDAK SAMA");
                        sameRed=0;
                    }
                    ctrCheckRed=0;
                }
                ctrCheckRed++;
                if(ctrCheckRed==1){
                    //animalBlue = fields[i][j].isi.name;
                    //lastiBlue= i;
                    //lastjBlue=j;
                    int ctr=0;
                    for (int k = 0; k < 9; k++) {
                        for (int l = 0; l < 7; l++) {
                            if(fields[k][l].isi!=null) {
                                if (fields[k][l].isi.color.equals("red")) {
                                    //System.out.println(fields[k][l].isi.name);

                                    map.put(fields[k][l].isi.name, ctr);
                                    //map.get("name"); // returns "demo"
                                }
                            }
                            ctr++;
                            //state.fields[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
                        }
                    }
                    if(sameRed==0){
                        sameRed++;
                    }
                }

                if(fields[8][3].isi!=null){
                    Intent moveActivity = new Intent(getApplicationContext(), MainActivity.class);
                    Toast.makeText(this, "Red Win!", Toast.LENGTH_SHORT).show();
                    startActivity(moveActivity);
                }

                turn = "blue";
                open.clear();
            }
        }
    }

    private void checkMove2(int i, int j, String turn){
        if(turn.equals("blue")){
            if(ctrCheckBlue==2 && sameBlue==3 && lastiBlue==i && lastjBlue==j && fields[this.i][this.j].isi.name.equals(animalBlue)){

            }else{
                open.add(addStep(i,j));
            }
        }else {
            if(ctrCheckRed==2 && sameRed==3 && lastiRed==i && lastjRed==j  && fields[this.i][this.j].isi.name.equals(animalRed)){

            }else{
                Field[][] field = new Field[9][7];
                for (int k = 0; k < 9; k++) {
                    for (int l = 0; l < 7; l++) {
                        field[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
                    }
                }
                field[this.i][this.j].isi = null;
                field[i][j] = new Field(fields[i][j].tipe,fields[this.i][this.j].isi);
//                System.out.println("Piece: "+field[i][j].isi.name);
//                System.out.println("i: "+i);
//                System.out.println("j: "+j);
                int ctr=0;
                Map<String, Integer> map = new HashMap<String, Integer>();
//                System.out.println("Map");
                for (int k = 0; k < 9; k++) {
                    for (int l = 0; l < 7; l++) {
                        if(field[k][l].isi!=null) {
                            if (field[k][l].isi.color.equals("red")) {
                                if(field[k][l].isi.name.equals("elephant")){
//                                    System.out.println("k elephant: "+k+", l elephant: "+l);
//                                    System.out.println("ctr: "+ctr);
                                }
                                //System.out.println(fields[k][l].isi.name);
                                map.put(field[k][l].isi.name, ctr);
                                //System.out.println(ctr);
                                //map.get("name"); // returns "demo"
                            }
                        }
                        ctr++;
                        //state.fields[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
                    }
                }


                if(this.map !=null && this.map.equals(map) && ctrCheckRed==2 && sameRed==3){

                }else{
                    open.add(addStep(i,j));
                    //sameRed++;
                }

//                if(!fields[i][j].isi.name.equals(animalRed)){
//                    sameRed=0;
//                    ctrCheckRed=0;
//                }
//                if(sameRed==3  && ctrCheckRed==2){
//                    sameRed=0;
//                    ctrCheckRed=0;
//                }
            }
        }
    }

    private void move(int i, int j){
        if(turn.equals("blue")){
            //System.out.println("blue");
            btnField[i][j].setImageResource(fields[this.i][this.j].isi.img);
            btnField[i][j].setBackgroundColor(fields[this.i][this.j].isi.colorId);
            defaultResField(this.i,this.j);
            try {
                fields[i][j].isi= (Piece) fields[this.i][this.j].isi.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            fields[this.i][this.j].isi = null;
        }
    }

    private State addStep(int i, int j){
        //System.out.println("red TURN");
//        state = new State();
//        for (int k = 0; k < 9; k++) {
//            for (int l = 0; l < 7; l++) {
//                state.fields[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
//            }
//        }
        //System.out.println("1,1: "+state.fields[1][1].isi.name);
        //System.out.println("1,1: "+stateX.fields[1][1].isi.name);
////            try {
        //System.out.println("i: "+i);
        //System.out.println("j: "+j);
        //System.out.println("this.i: "+this.i);
        //System.out.println("this.j: "+this.j);

        //System.out.println(fields[this.i][this.j].tipe);
        //System.out.println(fields[this.i][this.j].isi.name);
        stateX = new State();
        for (int k = 0; k < 9; k++) {
            for (int l = 0; l < 7; l++) {
                stateX.fields[k][l] = new Field(fields[k][l].tipe,fields[k][l].isi);
            }
        }
        stateX.fields[this.i][this.j].isi = null;
        stateX.fields[i][j] = new Field(fields[i][j].tipe,fields[this.i][this.j].isi);
        //System.out.println("Tipe tanah: "+stateX.fields[0][2].tipe);
        //Field f = new Field(fields[this.i][this.j].tipe,fields[this.i][this.j].isi);
//                stateX.fields[i][j] = new Field(fields[this.i][this.j].tipe,fields[this.i][this.j].isi);
        //stateX.fields[i][j].isi = (Piece) stateX.fields[this.i][this.j].isi.clone();
        //System.out.println(stateX.fields[i][j].isi.name);
        //System.out.println(stateX.fields[this.i][this.j].isi.name);
//            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
//            }
        stateX.fields[this.i][this.j].isi = null;
        //System.out.println(stateX.fields[i][j].isi.name);
        //try {
        //System.out.println("open.add()");
        return stateX;
        //open.add(stateX);
        //System.out.println("open nama: "+open.get(0).fields[0][1].isi.name);
//            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
//            }
    }


    private void defaultResField(int i, int j){
        btnField[i][j].setImageResource(android.R.color.transparent);
        if(i==0 && (j==2 || j==4) || i==1 && j==3 || i==7 && j==3 || i==8 && (j==2 || j==4)){
            btnField[i][j].setBackgroundColor(getResources().getColor(R.color.colorTrap));
            btnField[i][j].setImageResource(R.drawable.trap1);
        }else if(i==0 && j==3 || i==8 && j==3){
            btnField[i][j].setBackgroundColor(Color.RED);
            btnField[i][j].setImageResource(R.drawable.star1);
        }else if(j>=1 && j<=2 && (i>=3 && i<=5) || j>=4 && j<=5 && (i>=3 && i<=5)){
            btnField[i][j].setBackgroundResource(R.drawable.water1);
        }else{
            btnField[i][j].setBackgroundResource(R.drawable.grass1);
        }
    }

    private void refreshField(){
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 7; j++) {
                btnField[i][j].setImageResource(android.R.color.transparent);
                if(i==0 && (j==2 || j==4) || i==1 && j==3 || i==7 && j==3 || i==8 && (j==2 || j==4)){
                    btnField[i][j].setBackgroundColor(getResources().getColor(R.color.colorTrap));
                    btnField[i][j].setImageResource(R.drawable.trap1);
                }else if(i==0 && j==3 || i==8 && j==3){
                    btnField[i][j].setBackgroundColor(Color.RED);
                    btnField[i][j].setImageResource(R.drawable.star1);
                }else if(j>=1 && j<=2 && (i>=3 && i<=5) || j>=4 && j<=5 && (i>=3 && i<=5)){
                    btnField[i][j].setBackgroundResource(R.drawable.water1);
                }else{
                    btnField[i][j].setBackgroundResource(R.drawable.grass1);
                }
                if(fields[i][j].isi!=null){
//                    System.out.println("listsssss: "+fields[i][j].isi.name);
//                    if(fields[i][j].isi.name.equals("dog") && fields[i][j].isi.color.equals("red")){
//                        System.out.println("i: "+i);
//                        System.out.println("j: "+j);
//                    }
                    btnField[i][j].setImageResource(fields[i][j].isi.img);
                    btnField[i][j].setBackgroundColor(fields[i][j].isi.colorId);
                }
            }
        }
    }
}
